/**
 * A React Native Test
 * Made by Héctor Ferís
 * For Holded
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import List from '../components/list';
import Loading from '../components/loading';
import Header from '../components/header';
import store from '../redux/store';

export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = store.getState();
  }

  componentDidMount() {
    store.subscribe(() => {
      this.setState(store.getState());
    });
    NetInfo.fetch().then(state => {
      console.log("Connection type", state.type);
      console.log("Is connected?", state.isConnected);
      if (state.isConnected) { store.dispatch({ type: 'GET_DATA' }); }
      else { store.dispatch({ type: 'RETRIEVE_DATA' }); }
    });
  }

  onClick(action, id) {
    console.log('Click!', action, id);
    if (action === 'fav') {
      store.dispatch({ type: 'SET_FAV', id: id });
    }
    if (action === 'del') {
      store.dispatch({ type: 'DELETE_FAV', id: id });
    }
  }

  render() {
    const { dataSource, fetching } = this.state;
    return (
      <Fragment>
        <Header view="Home" />
        <ScrollView style={styles.scroll} contentInsetAdjustmentBehavior='automatic'>
          {fetching ? (
            <Loading />
          ) : (
              <List data={dataSource.sort((a, b) => b.fav - a.fav)} onClick={this.onClick} />
            )}
        </ScrollView>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  scroll: {
    backgroundColor: '#e4e4e4',
  },
});