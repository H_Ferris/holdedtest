/**
 * A React Native Test
 * Made by Héctor Ferís
 * For Holded
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import Loading from '../components/loading';
import Header from '../components/header';
import store from '../redux/store';

export default class Top extends React.Component {
  constructor(props) {
    super(props);
    this.state = store.getState();
  }

  componentDidMount() {
    store.subscribe(() => {
      this.setState(store.getState());
    });
    this.setState({ fetching: true });
    NetInfo.fetch().then(state => {
      console.log("Connection type", state.type);
      console.log("Is connected?", state.isConnected);
      if (state.isConnected) { store.dispatch({ type: 'GET_TOP' }); }
      else { store.dispatch({ type: 'RETRIEVE_TOP' }); }
    });
  }

  render() {
    const { dataTop, fetching } = this.state;
    return (
      <Fragment>
        <Header view="Top Gain" />
        {fetching ? (
          <Loading />
        ) : (dataTop &&
          <View style={styles.topContainer}>
            <Text style={styles.topSymbol}>Crypto Coin: {dataTop.symbol}</Text>
            <Text style={styles.topPrice}>Bid Price: {dataTop.bidPrice}</Text>
            <Text style={styles.topQty}>Bid Qty: {dataTop.bidQty}</Text>
          </View>
          )}
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  topContainer: {
    flex: 1,
    marginTop: 2,
    paddingHorizontal: 24,
    paddingVertical: 5,
    backgroundColor: '#e8b53d',
    flexDirection: 'column',
  },

  topSymbol: {
    fontSize: 26,
    fontWeight: 'bold',
    color: 'black',
  },
  topPrice: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: 'navy',
  },
  topQty: {
    marginTop: 8,
    fontSize: 16,
    fontWeight: '400',
    color: '#8f0200',
  },
});
