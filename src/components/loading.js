/**
 * A React Native Test
 * Made by Héctor Ferís
 * For Holded
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import {
  View,
  Image,
  StyleSheet,
} from 'react-native';

const Loading = () => {
  return (
    <Fragment>
      <View style={styles.container}>
        <Image style={styles.spinner} source={require('../assets/loading-gif.gif')} />
      </View>
    </Fragment>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignContent: 'center',
    justifyContent: 'center'
  },
  spinner: {
    alignSelf: 'center',
    height: 120,
    width: 120,
    margin: 100,
  },
});

export default Loading;
