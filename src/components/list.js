/**
 * A React Native Test
 * Made by Héctor Ferís
 * For Holded
 *
 * @format
 * @flow
 */

import React, {Fragment} from 'react';
import {
  FlatList,
} from 'react-native';

import Element from '../components/element';

const List = ({data, onClick}) => {
  return (
    <Fragment>
        <FlatList
          data={data}
          keyExtractor={item => item.symbol}
          renderItem={({item}) => <Element item={item} onClick={onClick} />}
          />
    </Fragment>
  );
};

export default List;
