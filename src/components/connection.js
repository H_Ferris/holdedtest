import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import NetInfo from '@react-native-community/netinfo';

export default class Connection extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            connected: false
        };
    }

    componentDidMount() {
        NetInfo.fetch().then(state => {
            if (state.isConnected) this.setState({ connected: true });
        });
    }

    render() {
        const { connected } = this.state;
        return (
            <View style={styles.view}>
                {connected ?
                    <Text style={styles.textCon}>Net OK</Text>
                    :
                    <Text style={styles.textNoCon}>No Net</Text>
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    view: {
        alignSelf: 'flex-end',
        backgroundColor: 'black',
        alignItems: 'center',
        right: 15
    },
    textCon: {
        lineHeight: 50,
        fontSize: 16,
        color: 'green',
    },
    textNoCon: {
        lineHeight: 50,
        fontSize: 16,
        color: 'red',
    },
});