/**
 * A React Native Test
 * Made by Héctor Ferís
 * For Holded
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
} from 'react-native';

const Element = ({ item, onClick }) => {
    return (
        <Fragment>
            <View style={[styles.elementContainer, item.fav && styles.elementFav]} key={item.symbol}>
                <View style={styles.coin}>
                    <Text style={styles.elementSymbol}>{item.baseAsset} ({item.symbol})</Text>
                    <Text style={styles.elementPrice}>Price: {item.price}</Text>
                </View>
                <View style={styles.buttons}>
                    {item.fav ?
                        <TouchableOpacity onPress={() => onClick('del', item.symbol)}>
                            <Text style={styles.del}>UnFav</Text>
                            <View style={styles.ballUnFav} />
                        </TouchableOpacity>
                        :
                        <TouchableOpacity onPress={() => onClick('fav', item.symbol)}>
                            <Text style={styles.fav}>Fav</Text>
                            <View style={styles.ballFav} />
                        </TouchableOpacity>
                    }
                </View>
            </View>
        </Fragment>
    );
};

const styles = StyleSheet.create({
    elementContainer: {
        marginTop: 2,
        paddingHorizontal: 24,
        paddingVertical: 5,
        backgroundColor: '#e8b53d',
        flexDirection: 'row',
    },
    elementFav: {
        backgroundColor: '#e89c3d',
    },
    coin: {
        flex: 1,
    },
    elementSymbol: {
        fontSize: 24,
        fontWeight: '600',
        color: 'black',
    },
    elementPrice: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
        color: '#0f3e57',
    },
    buttons: {
        flex: 0.2,
        alignItems: 'flex-end'
    },
    fav: {
        fontSize: 18,
        paddingVertical: 5,
        color: '#0f4003',
    },
    ballFav: {
        height: 20,
        width: 20,
        backgroundColor: '#0f4003',
        borderRadius: 10,
        alignSelf: 'center',
    },
    del: {
        fontSize: 18,
        paddingVertical: 5,
        color: '#8f0200',
    },
    ballUnFav: {
        height: 20,
        width: 20,
        backgroundColor: '#8f0200',
        borderRadius: 10,
        alignSelf: 'center',
    },
});

export default Element;
