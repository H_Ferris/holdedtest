/**
 * A React Native Test
 * Made by Héctor Ferís
 * For Holded
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import Connection from './connection';
import { withNavigation } from 'react-navigation';

class Header extends React.Component {
  render() {
    const { view } = this.props;
    return (
      <Fragment>
        <View style={styles.header}>
          <TouchableOpacity onPress={() => this.props.navigation.toggleDrawer()}>
            <Text style={styles.headMenu}>MENU</Text>
          </TouchableOpacity>
          <Text style={styles.headTitle}>{view}</Text>
          <Connection />
        </View>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    paddingTop: 20,
    height: 70,
    backgroundColor: 'black',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  headMenu: {
    lineHeight: 50,
    fontSize: 20,
    color: '#e89c3d',
    left: 15,
    alignSelf: 'flex-start'
  },
  headTitle: {
    lineHeight: 50,
    fontSize: 22,
    color: '#e4e4e4',
    fontWeight: 'bold',
    alignSelf: 'center'
  },
});

export default withNavigation(Header);
