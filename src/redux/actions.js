export const GET_DATA = 'GET_DATA';
export const RETRIEVE_DATA = 'RETRIEVE_DATA';
export const SET_FAV = 'SET_FAV';
export const DELETE_FAV = 'DELETE_FAV';
export const GET_TOP = 'GET_TOP';
export const RETRIEVE_TOP = 'RETRIEVE_TOP';
