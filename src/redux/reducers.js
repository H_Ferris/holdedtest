import { GET_DATA, RETRIEVE_DATA, SET_FAV, DELETE_FAV, GET_TOP, RETRIEVE_TOP } from './actions';
import { getData, setFav, deleteFav, getTop } from './functions';

const INITIAL_STATE = {
  fetching: true,
  favsData: [],
  dataTop: {},
  dataSource: []
};

function theReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case GET_DATA:
      return Object.assign({}, state, {
        fetching: false,
        favsData: action.favs,
        dataSource: getData(action.data, action.prices, action.favs),
      });
    case RETRIEVE_DATA:
      return Object.assign({}, state, {
        fetching: false,
        favsData: action.favs,
        dataSource: getData(action.data, action.prices, action.favs),
      });
    case SET_FAV:
      state.favsData.push(action.id);
      return Object.assign({}, state, {
        favsData: state.favsData,
        dataSource: setFav(state.dataSource, action.id),
      });
    case DELETE_FAV:
      return Object.assign({}, state, {
        favsData: state.favsData.filter(x => x !== action.id),
        dataSource: deleteFav(state.dataSource, action.id),
      });
    case GET_TOP:
      return Object.assign({}, state, {
        fetching: false,
        dataTop: getTop(action.top),
      });
    case RETRIEVE_TOP:
      return Object.assign({}, state, {
        fetching: false,
        dataTop: getTop(action.top),
      });
    default:
      return state;
  }
}

export default theReducer;
