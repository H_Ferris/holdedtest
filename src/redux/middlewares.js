import Binance from 'binance-api-react-native';
import { GET_DATA, RETRIEVE_DATA, SET_FAV, DELETE_FAV, GET_TOP, RETRIEVE_TOP } from './actions';
import AsyncStorage from '@react-native-community/async-storage';

const client = Binance();

// client.time().then(time => console.log('Time: ', time));

const theMiddleware = store => next => action => {

  console.log('Action triggered: ' + action.type, store.getState());

  switch (action.type) {
    case GET_DATA:
      client.exchangeInfo().then(response => {
        action.data = response.symbols;
        AsyncStorage.setItem('Crypto_Data', JSON.stringify(response.symbols));
        client.prices().then(response => {
          action.prices = response;
          AsyncStorage.setItem('Prices_Data', JSON.stringify(response));
          AsyncStorage.getItem('Favs_Data').then(response => {
            if (response !== null) {
              action.favs = JSON.parse(response);
            } else {
              action.favs = [];
              AsyncStorage.setItem('Favs_Data', JSON.stringify([]));
            }
            next(action);
            console.log('Action finished: ' + action.type, store.getState());
          });
        });
      });
      break;
    case RETRIEVE_DATA:
      AsyncStorage.getItem('Crypto_Data').then(response => {
        if (response !== null) {
          action.data = JSON.parse(response);
          AsyncStorage.getItem('Prices_Data').then(response => {
            if (response !== null) {
              action.prices = JSON.parse(response);
              AsyncStorage.getItem('Favs_Data').then(response => {
                if (response !== null) {
                  action.favs = JSON.parse(response);
                } else action.favs = [];
                next(action);
                console.log('Action finished: ' + action.type, store.getState());
              });
            }
          });
        }
      });
      break;
    case SET_FAV:
        AsyncStorage.getItem('Favs_Data').then(response => {
          if (response !== null) {
            const favs =  JSON.parse(response);
            favs.push(action.id);
            AsyncStorage.setItem('Favs_Data', JSON.stringify(favs));
          } 
          next(action);
          console.log('Action finished: ' + action.type, store.getState());
        });
      break;
    case DELETE_FAV:
      AsyncStorage.getItem('Favs_Data').then(response => {
        if (response !== null) {
          const favs =  JSON.parse(response);
          AsyncStorage.setItem('Favs_Data', JSON.stringify(favs.filter(x => x !== action.id)));
        } 
        next(action);
        console.log('Action finished: ' + action.type, store.getState());
      });
      break;
    case GET_TOP:
      client.allBookTickers().then(response => {
        action.top = response;
        next(action);
        console.log('Action finished: ' + action.type, store.getState());
      });
      break;
    case RETRIEVE_TOP:
      AsyncStorage.getItem('Top_Data').then(response => {
        if (response !== null) {
          action.top = JSON.parse(response);
          AsyncStorage.setItem('Top_Data', JSON.stringify(response));
        }
        next(action);
        console.log('Action finished: ' + action.type, store.getState());
      });
      break;
    default: next(action);
  }
}

export default theMiddleware;
