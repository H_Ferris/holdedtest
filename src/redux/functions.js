export function getData(data, prices, favs) {
    if (Array.isArray(data)) data.forEach((element, index) => {
        data[index].price = prices[element.symbol];
        data[index].fav = 0;
        if (Array.isArray(favs) && favs.length > 0 && favs.find(x => x === element.symbol)) { data[index].fav = 1; }
        data.sort((a, b) => b.fav - a.fav);
    });
    return data;
}

export function setFav(data, id) {
    data[data.findIndex(x => x.symbol === id)].fav = 1;
    return data;
}

export function deleteFav(data, id) {
    data[data.findIndex(x => x.symbol === id)].fav = 0;
    return data;
}

export function getTop(data) {
    let top = { bidPrice: 0 };
    for(let item in data){ 
        if(data[item].bidPrice > top.bidPrice) top = data[item]; 
    };
    return top;
}