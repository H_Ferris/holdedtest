import { createStore, applyMiddleware } from 'redux';
import theReducer from '../redux/reducers';
import theMiddleware from '../redux/middlewares';

const store = createStore(theReducer, applyMiddleware(theMiddleware));

export default store;