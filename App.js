/**
 * A React Native Test
 * Made by Héctor Ferís
 * For Holded
 *
 * @format
 * @flow
 */

import { createAppContainer } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';

import Home from './src/views/home';
import Favs from './src/views/favs';
import Top from './src/views/top';

const AppNavigator = createDrawerNavigator({
  Home: {
    screen: Home,
  },
  Favs: {
    screen: Favs,
  },
  Top: {
    screen: Top,
  },
}, {
  drawerBackgroundColor: 'black',
  drawerWidth: 150,
  contentOptions: {
    labelStyle: {
      color: '#e4e4e4',
      fontSize: 20,
      paddingLeft: 20,
    },
    activeLabelStyle: {
      color: '#e89c3d',
    },
  }
});

export default createAppContainer(AppNavigator);